const path = require("path")
const express = require("express")
const app = express()
const router = require("./routes/index")
const cors = require("cors")

app.use(cors())
app.use(router)
app.options("*", cors())

app.use(express.static(path.join(__dirname, 'tf')))

app.listen(4000, () => {
    console.log("App listening on 0.0.0.0:4000...")
})
