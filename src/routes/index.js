const express = require("express");
const router = express.Router();
const prediction = require("./prediction.router");

router.use("/prediction", prediction);
router.use("/cities", require("./cities.router"));

module.exports = router;
