const express = require("express");
const router = express.Router();
const predictionService = require("../services/prediction.service");
const dataCollectorService = require("../services/datacollector.service");

router.get("/", (req, res) => {
    const latitude = req.query.latitude;
    const longitude = req.query.longitude;

    if (!latitude || !longitude) {
        res.json({
            error: 'Required parameters: latitude, longitude'
        });
        return;
    }

    if (req.query.weather) {
        req.query["is_" + req.query.weather] = 1;
    }

    dataCollectorService(req).then(data => {
        res.json({ data })
    });
});

module.exports = router;
