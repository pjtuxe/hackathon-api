const express = require("express");
const router = express.Router();

router.get("/", require("../controllers/cities.controller").get);

module.exports = router;
