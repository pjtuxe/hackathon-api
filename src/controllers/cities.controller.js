var client = null;
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://root:gtsMTdPp3Q@mongodb.default.svc.cluster.local:27017/admin";

MongoClient.connect(url, function(err, db) {
    if (err) throw err;

    client = db.db("hackathon");
});

const get = function (req, res) {
    client.collection("cities").find().toArray().then(function (data) {
        res.json(data);
    });
}

module.exports.get = get;
