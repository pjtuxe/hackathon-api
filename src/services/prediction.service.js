const tf = require("@tensorflow/tfjs-node");

/**
 * @param {Array} data
 * @returns {Promise}
 */
const predict = (data) => {
    return new Promise((resolve, reject) => {
        tf.loadLayersModel('file://src/tf/tfjs/model.json').then(model => {
            var prediction = model.predict(tf.tensor([ data ])).dataSync();
            resolve(prediction[0] || 0);
        }).catch(reject);
    });
};

module.exports = predict;
