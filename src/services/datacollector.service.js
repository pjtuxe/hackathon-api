const http = require("http");
const mapPointsService = require("./prediction/map-points.service");
const dateService = require("./prediction/date.service");
const predictionService = require("./prediction.service");

var apiKeys = [
    "a11261621163df149428134e27946681",
    "c7d6486339a625c7bac2811a164d2a61",
    "2d74968e76447a670ff61fb26403ab6b",
    "c023415656e39b763b1ec760ce2e6195",
    "4890af268b524077b63a297588df667f",
    "7a8ef48b15097dcbd6f16601d35bc6e9",
    "d5ec5f06a2cc5a8bf4c32142b1071ea8",
    "1702f49f75e467d3d5cbd26dbf33aaa8",
    "6d41664a2766c3ee376ef357c28aafef",
    "b426c2aa178debefd5cfd30f204d7690",
    "7722a544e329c17d72b79ffd1f1faa22",
    "80062503dc06a1a604d0d02ab91123a8",
    "06256b871430d96a930632c7e87188b8",
    "f475019a99eb49b896597dbb66cc83ac",
    "552eeb7536ec8f6dc94b06b7fa9748b9",
    "cedabacb9ccbdb4dbb6811d19e3b50ec",
    "a4f06d2708f6cd78e035a762be4aa864",
    "432a03bb95b01a359a1cb57d0dc6cd80",
    "6641328f49e931e3b2392a2056454cfe",
    "c22eab98a02984181bb887eafb699611",
    "a5b2e51447aa9a2d4e2f2ca379356e20",
    "4b5ccb290d69495d4207d9ced891244e",
    "e6cf60120307800c53863b555e3c7fd0",
    "329404fb14d00067ca477b0be552b0b9",
    "423d8e98ffcdd0180d7d2baece1c4f55",
    "85c68bccaf261bbd7c59812349a27b26",
    "dabe34ca7975cb8bd92f13768178aeab",
    "84be85cf85bcb7e902412f4ffc8d8a25",
    "2914d450b1da980efd1744bcd6a8cbf8",
    "5792cd0b42fcdba6e6cb2f7ef829d6ef",
    "0459378fead845321b716ca589abd1f7",
    "96262f2df861d9eb34ebcadd8e4f850e",
    "e4ec7f700c122ab017f50f365723fbf3",
    "6a77588d8817afe918706b5662cd6262",
    "7097253741b6939b1d2df4b813d41e12",
    "66da5639118e1040a0d461169af2f31a",
    "5f280c69bbecfc1392ee73d1b436a57e",
    "e66263b81871c14321821414fb67bb26",
    "e11564e6854844c5dc847068e748b88b",
    "706d05fbfd4f72a8e0895b0dad3fdf8f",
    "efe1d6179d9000e45d0243e3e75949ee",
    "5753652ecfb08f2a8c546f84a9872849",
];

/**
 * @param lat
 * @param lng
 * @returns {*[]}
 */
const getRandomMapPointsFor = (lat, lng) => {
    var result = [];

    [6, 12, 18].forEach(kilometer => {
        mapPointsService
            .getPoligionCoords([lat, lng], kilometer, 12)
            .forEach(coord => {
                //result.push([coord[0], coord[1]]);
                result.push([coord[1], coord[0]]);
            });
    });

    return result;
};

/**
 * @returns {[]}
 */
const getTimestamps = () => {
    // 10800 is 3 hours in seconds
    var date = new Date();
    date.setHours(date.getHours() - 1);
    return dateService.getTimestampsFrom(date, 5, 10800);
};

const getColorForConfidence = confidence => {
    if (confidence > 0.6) {
        return "red";
    } else if (confidence > 0.3) {
        return "orange";
    } else if (confidence > 0.1) {
        return "yellow"
    } else {
        return "transparent";
    }
};

const apiConfigs = {};

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var lastApiKey = 0;

const getConfigFromApi = (latitude, longitude, callback) => {
    var key = latitude + ":" + longitude;

    if (apiConfigs[key]) {
        console.log("cache")
        callback(apiConfigs[key]);
    } else {
        console.log("req")
        var apiKey = apiKeys[lastApiKey];

        if (lastApiKey === apiKeys.length - 1) {
            lastApiKey = 0;
        } else {
            lastApiKey++;
        }

        var request = {
            hostname: 'api.openweathermap.org',
            port: 80,
            path: `/data/2.5/forecast?lat=${latitude}&lon=${longitude}&appid=${apiKey}`,
        };

        var req = http.get(request, (res) => {
            if (res.statusCode !== 200) {
                //console.error(request)
                console.error(res.statusMessage)
            }

            var body = '';

            res.on('data', data => {
                body += data;
            });

            res.on('end', function () {
                var response = JSON.parse(body);
                apiConfigs[key] = response.list;
                callback(apiConfigs[key]);
            });
        });

        req.on('error', error => {
            callback(error);
        });

        req.end()
    }
};

const getConfigFor = (configs, timestamp) => {
    var date = new Date(timestamp * 1000);
    var requestedDate = date.toISOString().substr(0, 10);
    var requestedTime = date.toISOString().substr(11, 2) + ":00";

    for (var i in configs) {
        if (configs.hasOwnProperty(i)) {
            var config = configs[i];
            var configDate = config.dt_txt.substr(0, 10);
            var configTime = config.dt_txt.substr(11, 5);

            if (configDate == requestedDate && configTime == requestedTime) {
                return config;
            }
        }
    }


    return null;
};

const collectDataFromApi = (req, callback) => {
    var result = [], loaded = 0;

    // Load cache from API
    getRandomMapPointsFor(req.query.latitude, req.query.longitude).forEach(point => {
        getConfigFromApi(point[1], point[0], () => {
            loaded++;
        })
    });

    var interval = setInterval(
        function () {
            if (loaded >= 32) {
                clearInterval(interval);

                getTimestamps().forEach(timestamp => {
                    var entry = {
                        date: new Date(timestamp * 1000).toISOString().substr(0, 10),
                        time: new Date(timestamp * 1000).toISOString().substr(11, 5),
                        alerts: [],
                    };

                    getRandomMapPointsFor(req.query.latitude, req.query.longitude).forEach(point => {
                        getConfigFromApi(point[1], point[0], (config) => {
                            var config_ = getConfigFor(config, timestamp);

                            if (!config_) {
                                //throw "Failed to get config for ";// + JSON.stringify(config);
                                return;
                            }

                            var predData = {
                                temp: config_.main.temp !== undefined ? config_.main.temp : 293.31,
                                feels_like: config_.main.feels_like !== undefined ? config_.main.feels_like : 289.39,
                                pressure: config_.main.pressure !== undefined ? config_.main.pressure : 1024.0,
                                humidity: config_.main.humidity !== undefined ? config_.main.humidity : 20.0,
                                dew_point: 270.06,
                                clouds: config_.clouds.all !== undefined ? config_.clouds.all : 1.0,
                                visibility: config_.visibility !== undefined ? config_.visibility : 16093.0,
                                wind_speed: config_.wind.speed !== undefined ? config_.wind.speed : 2.1,
                                wind_deg: config_.wind.deg !== undefined ? config_.wind.deg : 270.0,
                                elevation: 1444.0,
                                is_clear: config_.weather.main !== undefined && config_.weather.main === 'Clear' ? 1 : 0,
                                is_clouds: config_.weather.main !== undefined && config_.weather.main === 'Clouds' ? 1 : 0,
                                is_drizzle: config_.weather.main !== undefined && config_.weather.main === 'Drizzle' ? 1 : 0,
                                is_fog: config_.weather.main !== undefined && config_.weather.main === 'Fog' ? 1 : 0,
                                is_haze: config_.weather.main !== undefined && config_.weather.main === 'Haze' ? 1 : 0,
                                is_mist: config_.weather.main !== undefined && config_.weather.main === 'Mist' ? 1 : 0,
                                is_rain: config_.weather.main !== undefined && config_.weather.main === 'Rain' ? 1 : 0,
                                is_smoke: config_.weather.main !== undefined && config_.weather.main === 'Smoke' ? 1 : 0,
                                is_thunderstorm: config_.weather.main !== undefined && config_.weather.main === 'Thunderstorm' ? 1 : 0,
                            };

                            predictionService(Object.values(predData)).then(confidence => {
                                entry.alerts.push({
                                    coords: point,
                                    config: predData,
                                    confidence,
                                    color: getColorForConfidence(confidence)
                                });
                            });
                        });
                    });

                    result.push(entry)
                });

                var interval2 = setInterval(function () {
                    if (result.length >= 5) {
                        var ok = true;

                        for (var i in result) {
                            if (result.hasOwnProperty(i)) {
                                if (!result[i].alerts || result[i].alerts.length < 12) {
                                    ok = false;
                                }
                            }
                        }

                        if (ok) {
                            callback(result);
                            clearInterval(interval2);
                        }
                    }
                }, 500);
            }
        },
        500
    );
};

const getRandom = (min, max) => {
    return (Math.random() * (min - max) + max).toFixed(4);
};

const collectDataFromRequest = (req) => {
    return new Promise(resolve => {
        var result = [], config = {
            temp: req.query.temp !== undefined ? req.query.temp : 293.31,
            feels_like: req.query.feels_like !== undefined ? req.query.feels_like : 289.39,
            pressure: req.query.pressure !== undefined ? req.query.pressure : 1024.0,
            humidity: req.query.humidity !== undefined ? req.query.humidity : 20.0,
            dew_point: req.query.dew_point !== undefined ? req.query.dew_point : 270.06,
            clouds: req.query.clouds !== undefined ? req.query.clouds : 1.0,
            visibility: req.query.visibility !== undefined ? req.query.visibility : 16093.0,
            wind_speed: req.query.wind_speed !== undefined ? req.query.wind_speed : 2.1,
            wind_deg: req.query.wind_deg !== undefined ? req.query.wind_deg : 270.0,
            elevation: req.query.elevation !== undefined ? req.query.elevation : 1444.0,
            is_clear: req.query.is_clear !== undefined ? req.query.is_clear : 0,
            is_clouds: req.query.is_clouds !== undefined ? req.query.is_clouds : 0,
            is_drizzle: req.query.is_drizzle !== undefined ? req.query.is_drizzle : 0,
            is_fog: req.query.is_fog !== undefined ? req.query.is_fog : 0,
            is_haze: req.query.is_haze !== undefined ? req.query.is_haze : 0,
            is_mist: req.query.is_mist !== undefined ? req.query.is_mist : 0,
            is_rain: req.query.is_rain !== undefined ? req.query.is_rain : 0,
            is_smoke: req.query.is_smoke !== undefined ? req.query.is_smoke : 0,
            is_thunderstorm: req.query.is_thunderstorm !== undefined ? req.query.is_thunderstorm : 0
        };

        predictionService(Object.values(config)).then(confidence => {
            getTimestamps().forEach(function (timestamp) {
                result.push({
                    date: new Date(timestamp * 1000).toISOString().substr(0, 10),
                    time: new Date(timestamp * 1000).toISOString().substr(11, 5),
                    alerts: getRandomMapPointsFor(req.query.latitude, req.query.longitude)
                        .reduce((carry, point) => {
                            confidence = Math.min(1, confidence * getRandom(0.4, 1.75));

                            carry.push({
                                coords: point,
                                config,
                                confidence,
                                color: getColorForConfidence(confidence)
                            });
                            return carry;
                        }, [])
                })
            });

            resolve(result);
        });
    });
};

/**
 * @param req
 * @returns {Promise}
 */
const collect = (req) => {
    return new Promise(resolve => {
        return (req.query.use_prediction == 1 || req.query.use_prediction == "true")
            ? collectDataFromApi(req, function (data) {
                resolve(data)
            })
            : collectDataFromRequest(req).then(resolve);
    });
};

module.exports = collect;
