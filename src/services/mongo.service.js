var client = null;
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://root:gtsMTdPp3Q@mongodb.default.svc.cluster.local:27017/admin";

module.exports = new Promise(function (resolve, reject) {
    async.function(function () {
        MongoClient.connect(url, function(err, db) {
            if (err) throw err;

            client = db.db("hackathon");
            resolve(client);
        });
    });
});
