/**
 * @param {Date} from
 * @param {Number} num
 * @param {Number=} interval
 */
const getTimestampsFrom = (from, num, interval) => {
    interval = interval || 3600;

    while ([0, 3, 6, 9, 12, 15, 18, 21].indexOf(from.getHours())) {
        from.setHours(from.getHours() + 1);
    }

    var timestamp = from.getTime() / 1000, result = [];

    for (var i = 1; i <= num; i++) {
        result.push((timestamp + interval * i).toFixed(0));
    }

    return result;
};

module.exports = {
    getTimestampsFrom
};
