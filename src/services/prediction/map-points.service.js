
/**
 * Params: [[latitude, longitude]]
 *
 * @param params
 * @param distance
 * @param segments
 * @returns {[]}
 */
function getPoligionCoords(params, distance = 6.21371, segments = 32) {
    var locs = [];
    var a = 360 / segments;

    if (!Array.isArray(params[0])) {
        params[0] = params[0] * Math.PI / 180.0;
        params[1] = params[1] * Math.PI / 180.0;
        for (var x = 0; x < segments; x++) {
            var result = convertGPS(params, distance, x * a);
            locs.push([result[0], result[1]]);
        }
    }

    return locs;
}

/**
 * @param params
 * @param distance
 * @param deg
 * @returns {number[]}
 */
function convertGPS(params, distance, deg) {
    distance = distance * 1.60934 / 3956;
    var tc = (deg / 90) * Math.PI / 2;
    var lat = Math.asin(
        Math.sin(params[0]) * Math.cos(distance) + Math.cos(params[0]) * Math.sin(distance) * Math.cos(tc)
    );
    var lon;
    if (Math.cos(params[0]) === 0) {
        lon = params[1];
    } else {
        lon = ((params[1] - Math.asin(Math.sin(tc) * Math.sin(distance) / Math.cos(params[0])) + Math.PI) % (2 * Math.PI)) - Math.PI;
    }
    lat = 180.0 * lat / Math.PI;
    lon = 180.0 * lon / Math.PI;
    return [lat, lon];
}

module.exports = {
    getPoligionCoords
};
